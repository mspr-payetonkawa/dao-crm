package cours.mspr.da.crm.dao;

import cours.mspr.da.crm.dto.Customer;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(value = "feign-client", url = "https://615f5fb4f7254d0017068109.mockapi.io/api/v1")
public interface DaoCrmFeign {
    @GetMapping(value = "/customers")
    List<Customer> listeCustomers();

    @GetMapping(value = "/customers/{id}")
    Customer recupererUnCustomer(@PathVariable("id") int id);
}
