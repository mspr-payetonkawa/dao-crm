package cours.mspr.da.crm.dto;

public class profileCustomer {
    String firstName;
    String lastName;

    /* GETTER / SETTER */

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
