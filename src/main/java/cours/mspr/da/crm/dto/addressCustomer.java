package cours.mspr.da.crm.dto;

public class addressCustomer {
    String postalCode;
    String city;

    /* GETTER / SETTER */

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
