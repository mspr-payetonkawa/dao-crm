package cours.mspr.da.crm.dto;

import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class Customer {
    String createdAt;
    String name;
    String username;
    String firstName;
    String lastName;
    addressCustomer address;
    profileCustomer profile;
    companyCustomer company;
    String id;
    String email;
    List<orderCustomer> orders;

    /* GETTER / SETTER */

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public addressCustomer getAddress() {
        return address;
    }

    public void setAddress(addressCustomer address) {
        this.address = address;
    }

    public profileCustomer getProfile() {
        return profile;
    }

    public void setProfile(profileCustomer profile) {
        this.profile = profile;
    }

    public companyCustomer getCompany() {
        return company;
    }

    public void setCompany(companyCustomer company) {
        this.company = company;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<orderCustomer> getOrders() {
        return orders;
    }

    public void setOrders(List<orderCustomer> orders) {
        this.orders = orders;
    }
}
